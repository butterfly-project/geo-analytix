package com.nibss.geoanalytix.config;

import com.elixir.fuse.springframework.config.AppConstant;
import com.elixir.fuse.springframework.config.ConfigProperties;
import org.aeonbits.owner.Config;

import static org.aeonbits.owner.Config.DisableableFeature.PARAMETER_FORMATTING;

/**
 * Created by elixir on 7/10/16.
 */
@Config.DisableFeature({PARAMETER_FORMATTING})
public interface AppConfig extends ConfigProperties{

    @Key("app.reportDir")
    @DefaultValue("${"+AppConstant.APP_HOME_KEY+"}/report")
    public String reportDir();

   /* @Key("app.report.inputDir")
    @DefaultValue("${app.reportDir}/input")
    public String reportInputDir();

    @Key("app.report.outputDir")
    @DefaultValue("${app.reportDir}/output")
    public String reportOutputDir();*/

    /*@Key("app.report.dateRange")
    @Pattern("dd/MM/yyyy")
    @Separator("-")
    @ConverterClass(ConfigConverter.DateConverter.class)
    public Date[] reportDateRange();*/
}
