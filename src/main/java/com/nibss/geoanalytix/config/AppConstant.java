package com.nibss.geoanalytix.config;

/**
 * Created by elixir on 7/12/16.
 */
public interface AppConstant {
    public static String DEFAULT_DATE_PATTERN = "dd/MM/yyyy";
    public static String DEFAULT_DATETIME_PATTERN = "dd/MM/yyyy HH mm ss";
    public static String FULL_NAMED_DATE_PATTERN = "ddMMyyyyHHmmss";
    public static Long NIBSS_INSTITUTION_ID = 1L;
    public static final String APP_NAME = "geo-analytix";
    public static final String PERSISTENCE_UNIT_NAME = APP_NAME;
    public static final String BVN_PERSISTENCE_UNIT_NAME = "bvn";
}
