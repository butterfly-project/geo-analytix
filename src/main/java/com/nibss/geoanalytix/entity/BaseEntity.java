package com.nibss.geoanalytix.entity;

import com.elixir.collab.model.Model;
import com.nibss.geoanalytix.model.Flag;
import com.nibss.geoanalytix.model.Flagable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Created by elixir on 8/21/16.
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable, Flagable,Model {
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = true)
    @Column(name = "longitude")
    private Double longitude;
    @Basic(optional = true)
    @Column(name = "latitude")
    private Double latitude;
    @Basic(optional = false)
    @Column(name = "flag")
    private int flag;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Flag getFlag() {
        return Flag.getFlag(flag);
    }

    public void setFlag(Flag flag) {
        this.flag = flag != null ? flag.getId() : Flag._ID;
    }
}
