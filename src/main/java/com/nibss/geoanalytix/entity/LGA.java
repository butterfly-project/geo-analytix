package com.nibss.geoanalytix.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by elixir on 8/21/16.
 */
@Entity
@Table(name = "LGA")
@XmlRootElement
public class LGA extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "lga_id")
    private Long lgaId;
    @Basic(optional = false)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "longitide")
    private double longitide = 0;
    @Basic(optional = false)
    @Column(name = "latitude")
    private double latitude = 0;

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lgaId != null ? lgaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LGA)) {
            return false;
        }
        LGA other = (LGA) object;
        if ((this.lgaId == null && other.lgaId != null) || (this.lgaId != null && !this.lgaId.equals(other.lgaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getName() + "[ lgaId=" + lgaId + " ]";
    }

    @XmlTransient
    @Override
    public Object getModelId() {
        return lgaId;
    }
}
