package com.nibss.geoanalytix.bvn.repository;

import com.nibss.geoanalytix.bvn.entity.Demography;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by oogunjimi on 8/23/2016.
 */

public interface DemographyRepository extends JpaRepository<Demography, Long> {
}
