/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nibss.geoanalytix.bvn.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author oogunjimi
 */
@Entity
@Table(name = "demography")
public class Demography implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "bvn")
    private String bvn;
    @Column(name = "marital_status")
    private String maritalStatus;
    @Column(name = "gender")
    private String gender;
    /*@Column(name = "surname")
    private String surname;
    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "enroll_user_name")
    private String enrollUserName;*/
    @Column(name = "nationality")
    private String nationality;
    @Column(name = "state_of_origin")
    private String stateOfOrigin;
    @Column(name = "lga_of_origin")
    private String lgaOfOrigin;
    @Column(name = "state_of_residence")
    private String stateOfResidence;
    @Column(name = "lga_of_residence")
    private String lgaOfResidence;
    @Column(name = "state_of_capture")
    private String stateOfCapture;
    @Column(name = "lga_of_capture")
    private String lgaOfCapture;
    @Column(name = "residential_address")
    private String residentialAddress;

    /* @Column(name = "email")
    private String email;
    @Column(name = "nin")
    private String nin;
    @Column(name = "landmarks")
    private String landmarks;
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "Phone_number1")
    private String phonenumber1;
    @Basic(optional = false)
    @Column(name = "phone_number2")
    private String phoneNumber2;
    @Column(name = "enroll_agency")
    private String enrollAgency;
    @Column(name = "enrollment_date")
    private String enrollmentDate;
    @Column(name = "enroll_bank_code")
    private String enrollBankCode;
    @Column(name = "customer_id")
    private String customerId;
    @Column(name = "date_of_birth")
    private String dateOfBirth;
    @Column(name = "name_on_card")
    private String nameOnCard;
    @Column(name = "title")
    private String title;
    @Column(name = "branch_name")
    private String branchName;
    @Column(name = "watchlisted")
    private Integer watchlisted;
    @Basic(optional = false)
    @Column(name = "serial_no")
    private int serialNo;*/

    public Demography() {
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getStateOfOrigin() {
        return stateOfOrigin;
    }

    public void setStateOfOrigin(String stateOfOrigin) {
        this.stateOfOrigin = stateOfOrigin;
    }

    public String getLgaOfOrigin() {
        return lgaOfOrigin;
    }

    public void setLgaOfOrigin(String lgaOfOrigin) {
        this.lgaOfOrigin = lgaOfOrigin;
    }

    public String getStateOfResidence() {
        return stateOfResidence;
    }

    public void setStateOfResidence(String stateOfResidence) {
        this.stateOfResidence = stateOfResidence;
    }

    public String getLgaOfResidence() {
        return lgaOfResidence;
    }

    public void setLgaOfResidence(String lgaOfResidence) {
        this.lgaOfResidence = lgaOfResidence;
    }

    public String getStateOfCapture() {
        return stateOfCapture;
    }

    public void setStateOfCapture(String stateOfCapture) {
        this.stateOfCapture = stateOfCapture;
    }

    public String getLgaOfCapture() {
        return lgaOfCapture;
    }

    public void setLgaOfCapture(String lgaOfCapture) {
        this.lgaOfCapture = lgaOfCapture;
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bvn != null ? bvn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Demography)) {
            return false;
        }
        Demography other = (Demography) object;
        if ((this.bvn == null && other.bvn != null) || (this.bvn != null && !this.bvn.equals(other.bvn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getName()+"[ bvn=" + bvn + " ]";
    }
    
}
