package com.nibss.geoanalytix.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;

/**
 * Created by elixir on 5/17/16.
 */
@Scope(value = WebApplicationContext.SCOPE_SESSION,proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class NavigatorBean implements Serializable {

    private boolean navCollapsed = false;
    private String moduleName;
    private String resourceId;
    private String viewId;
    private String viewPath;

    public boolean isNavCollapsed() {
        return navCollapsed;
    }

    public void setNavCollapsed(boolean navCollapsed) {
        this.navCollapsed = navCollapsed;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public String getViewPath() {
        return viewPath;
    }

    public void setViewPath(String viewPath) {
        this.viewPath = viewPath;
    }

    public void setView(String moduleName,String viewPath){
        this.moduleName=moduleName;
        this.viewPath=viewPath;
    }

    public boolean inView(String partialView){
        return viewPath!=null && viewPath.startsWith(partialView);
    }
}
