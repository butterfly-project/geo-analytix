package com.nibss.geoanalytix.bean;

import com.nibss.geoanalytix.bvn.repository.DemographyRepository;
import com.nibss.geoanalytix.entity.Country;
import com.nibss.geoanalytix.entity.District;
import com.nibss.geoanalytix.entity.State;
import com.nibss.geoanalytix.repository.CountryRepository;
import com.nibss.geoanalytix.repository.DistrictRepository;
import com.nibss.geoanalytix.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by elixir on 8/21/16.
 */
public class GeoCodingBean {
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private StateRepository stateRepository;
    @Autowired
    private DistrictRepository districtRepository;
    @Autowired
    private DemographyRepository demographyRepository;

    private Map<String,Country> countryMap=new ConcurrentHashMap<>();
    private Map<String,State> stateMap=new ConcurrentHashMap<>();
    private Map<String,District> districtMap=new ConcurrentHashMap<>();
    public GeoCodingBean(){
    }

   /* public void initMap(){
        Map<BaseRepository<? extends BaseEntity>,Map<String,? extends Object>> maps=new ConcurrentHashMap<>();
        maps.put(countryRepository,countryMap);
        maps.put(stateRepository,stateMap);
        maps.put(districtRepository,districtMap);
        Object[][] data={new Object[]{countryRepository,countryMap}};
        ExecutorService executorService = Executors.newFixedThreadPool(data.length);
        List<CompletableFuture<Void>> futures = maps.entrySet().stream().map(entry -> CompletableFuture.runAsync(() -> {
            List<? extends BaseEntity> entities = entry.getKey().findAll();
            entities.stream().forEach(entity -> {
                entry.getValue().put(entity.getName(), entity);
            });
        }, executorService)).map(future->future.exceptionally()).collect(Collectors.toList());
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).whenComplete((voids,throwables)->{

        });
    }
    public void load(){
        List<Country> countries = countryRepository.findAll();
        countries.stream().forEach(country -> {
            countryMap.put(country.getName(),country);
        });
    }*/

    public void v(){
        demographyRepository.findAll()
    }
}
