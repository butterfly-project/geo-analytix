package com.nibss.geoanalytix.bean;

import com.elixir.collab.data.DataResponse;
import com.elixir.collab.data.DataValidationException;
import com.elixir.collab.data.FieldMessage;
import com.elixir.collab.data.Message;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;
import java.util.List;

/**
 * Created by elixir on 5/29/16.
 */

@ControllerAdvice(annotations = Controller.class)
public class ExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

    /*@ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity exception(Exception exception, WebRequest request) {
        return new ApiError(Throwables.getRootCause(exception).getMessage());
    }*/

    /*@ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value=HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorFormInfo handleMethodArgumentNotValid(HttpServletRequest req, MethodArgumentNotValidException ex) {

        String errorMessage = localizeErrorMessage("error.bad.arguments");
        String errorURL = req.getRequestURL().toString();

        ErrorFormInfo errorInfo = new ErrorFormInfo(errorURL, errorMessage);

        BindingResult result = ex.getBindingResult();

        List<FieldError> fieldErrors = result.getFieldErrors();

        errorInfo.getFieldErrors().addAll(populateFieldErrors(fieldErrors));

        return errorInfo;
    }*/

    @ExceptionHandler({DataValidationException.class})
    public final ResponseEntity<Object> handleValidationException(Exception ex, WebRequest request) {
        DataResponse dataResponse = new DataResponse();
        if (ex instanceof DataValidationException) {
            DataValidationException exception = (DataValidationException) ex;
            Message[] messages = exception.getMessages();
            if (messages != null) {
                Arrays.stream(messages).forEach(msg -> {
                    dataResponse.addMessage(msg);
                });
            }
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return super.handleExceptionInternal(ex, dataResponse, headers, HttpStatus.OK, request);
    }

    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        BindingResult result = exception.getBindingResult();
        List<ObjectError> errors = result == null ? null : result.getAllErrors();
        DataResponse dataResponse = new DataResponse();
        if (errors != null) {
            errors.stream().forEach(error -> {
                if (error instanceof FieldError) {
                    FieldError fieldError = (FieldError) error;
                    dataResponse.addMessage(new FieldMessage(fieldError.getField(), fieldError.getDefaultMessage(), Message.Severity.ERROR));
                } else
                    dataResponse.addMessage(new Message(error.getDefaultMessage(), Message.Severity.ERROR));
            });
        }

        headers.setContentType(MediaType.APPLICATION_JSON);
        return super.handleExceptionInternal(exception, dataResponse, headers, status, request);
    }
}

