/*
 * copyright 2014 Nibss
 * http://www.nibss-plc.com/
 * 
 */
package com.nibss.geoanalytix.model;

/**
 * @author Oluwaseun Ogunjimi
 */
public enum  Resource {

    CREATE(Resource.CREATE_VALUE);


    private String resourceName;

   Resource(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceName() {
        return resourceName;
    }
    public static final String MODULE_NAME = "core";
    public static final String CREATE_VALUE= "create";
    public static final String EDIT_VALUE = "edit";
    public static final String DELETE_VALUE = "delete";
    //public static final String READONLY = "readonly";
    public static final String VIEW_VALUE = "view";
    public static final String LIST_VALUE = "list";

    public static final String DATATABLE_VALUE = "datatable";
    //
    public static final String RESET_PASSWORD_VALUE = "reset-password";
    public static final String CHANGE_PASSWORD_VALUE = "change-password";
    public static final String HOME_VALUE = "home";

}
