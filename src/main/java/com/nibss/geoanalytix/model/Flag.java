package com.nibss.geoanalytix.model;

import java.util.Arrays;

/**
 * Created by elixir on 5/27/16.
 */
//@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Flag {
    _(Flag._ID),
    NO(Flag.NO_ID),
    YES(Flag.YES_ID),
    DISABLED(Flag.DISABLED_ID),
    ENABLED(Flag.ENABLED_ID),
    INACTIVE(Flag.INACTIVE_ID),
    ACTIVE(Flag.ACTIVE_ID),
    LOCKED(Flag.LOCKED_ID),
    EXPIRED(Flag.EXPIRED_ID),
    DELETED(Flag.DELETED_ID),
    OPEN(Flag.OPEN_ID),
    CLOSED(Flag.CLOSED_ID),
    PROCESSING(Flag.PROCESSING_ID);


    private int id;

    Flag(int id) {
        this.id = id;
    }

    //@JsonView(DataTablesOutput.View.class)
    public int getId() {
        return id;
    }
/*  @JsonView(DataTablesOutput.View.class)
      private String name;
      @JsonView(DataTablesOutput.View.class)
      private int ordinal;*/

   // @JsonView(DataTablesOutput.View.class)
    public String getName() {
        return name();
    }

   // @JsonView(DataTablesOutput.View.class)
    public int getOrdinal() {
        return this.ordinal();
    }

    public static final int _ID = 0;
    public static final int NO_ID = 1;
    public static final int YES_ID = 2;
    public static final int DISABLED_ID = 3;
    public static final int ENABLED_ID = 4;
    public static final int INACTIVE_ID = 5;
    public static final int ACTIVE_ID = 6;
    public static final int LOCKED_ID = 7;
    public static final int EXPIRED_ID = 8;
    public static final int DELETED_ID = 9;
    public static final int OPEN_ID = 10;
    public static final int CLOSED_ID = 11;
    public static final int PROCESSING_ID = 11;

    public static Flag getFlag(int id) {
        return Arrays.stream(Flag.values()).filter(flag -> flag.getId() == id).findFirst().orElse(Flag._);
    }
}
