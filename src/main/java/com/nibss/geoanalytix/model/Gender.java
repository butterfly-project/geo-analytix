package com.nibss.geoanalytix.model;

/**
 * Created by elixir on 8/22/16.
 */
public enum Gender {
    MALE(1),
    FEMALE(2);

    private int id;

    Gender(int id) {
        this.id = id;
    }
}
