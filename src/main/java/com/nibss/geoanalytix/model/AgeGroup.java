package com.nibss.geoanalytix.model;

/**
 * Created by elixir on 8/22/16.
 */
public enum AgeGroup {
    TODDLER(1,10),
    TEENAGE(11,18),
    ADULT(19,200);


    private final int minAge;
    private final int maxAge;

    AgeGroup(int minAge, int maxAge) {
        this.minAge = minAge;
        this.maxAge = maxAge;
    }
}
