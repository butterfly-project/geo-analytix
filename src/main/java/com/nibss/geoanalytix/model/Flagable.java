package com.nibss.geoanalytix.model;

/**
 * Created by elixir on 6/13/16.
 */
public interface Flagable {
    void setFlag(Flag flag);
    Flag getFlag();
}
