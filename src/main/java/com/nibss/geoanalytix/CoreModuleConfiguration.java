package com.nibss.geoanalytix;

import com.elixir.collab.springframework.CollabConfiguration;
import com.elixir.fuse.springframework.context.Module;
import com.elixir.fuse.springframework.persistence.PersistenceUnit;
import com.nibss.geoanalytix.bvn.entity.Demography;
import com.nibss.geoanalytix.config.AppConstant;
import com.nibss.geoanalytix.entity.Country;
import com.nibss.geoanalytix.model.Resource;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Import;

/**
 * Created by elixir on 8/21/16.
 */
@Import({CollabConfiguration.class})
@Module(name = Resource.MODULE_NAME, autoRegister = true, urlPatterns = "/"
        , persistence = {@PersistenceUnit(unitName = AppConstant.PERSISTENCE_UNIT_NAME, entityScan = @EntityScan(basePackageClasses = Country.class))
,@PersistenceUnit(unitName = AppConstant.BVN_PERSISTENCE_UNIT_NAME, entityScan = @EntityScan(basePackageClasses = Demography.class))}
)
public class CoreModuleConfiguration {


}
