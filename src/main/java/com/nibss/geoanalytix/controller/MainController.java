package com.nibss.geoanalytix.controller;


import com.nibss.geoanalytix.entity.Country;
import com.nibss.geoanalytix.model.Resource;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by elixir on 5/17/16.
 */
@Scope(value = WebApplicationContext.SCOPE_SESSION)
@Controller
@RequestMapping("/")
public class MainController extends BaseController<Country> {

    //@Autowired
    private PasswordEncoder passwordEncoder;

    public String index(String viewId) {
        String resourceId;
        switch ((viewId == null || viewId.isEmpty()) ? Resource.HOME_VALUE : viewId) {
            case Resource.CHANGE_PASSWORD_VALUE: {
                resourceId = Resource.CHANGE_PASSWORD_VALUE;
                break;
            }
            default: {
                resourceId = Resource.HOME_VALUE;
                break;
            }
        }
        return resolveView(resourceId);
    }

}
