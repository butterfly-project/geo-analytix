package com.nibss.geoanalytix.controller;


import com.elixir.collab.data.*;
import com.elixir.collab.model.Model;
import com.elixir.fuse.springframework.context.ApplicationManager;
import com.elixir.fuse.springframework.context.Module;
import com.github.vbauer.herald.annotation.Log;
import com.nibss.geoanalytix.bean.NavigatorBean;
import com.nibss.geoanalytix.model.Flag;
import com.nibss.geoanalytix.model.Flagable;
import com.nibss.geoanalytix.model.Resource;
import com.nibss.geoanalytix.repository.BaseRepository;
import com.nibss.geoanalytix.repository.FetchableRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by elixir on 5/9/16.
 */

public abstract class BaseController<M extends Model> implements Serializable {
    @Log
    private Logger logger;
    @Inject
    private HttpServletRequest request;
    @Autowired(required = false)
    private BaseRepository<M> repository;
    /*@Autowired(required = false)
    private DataTableBaseController<M> dataTableController;*/
    @Inject
    private NavigatorBean navigatorBean;
    private M model;
    private Class<M> modelClass;
    //private String viewPath;
    private String moduleName;
    private String controllerPath;
    private String modulePath;
    private String displayName;
    //
    @Inject
    private ApplicationManager applicationManager;

    private String[] pathNames;
    private String PATH_SEPARATOR = "/";
    protected final static String MAIN_VIEW = "main";


    public BaseController() {
        this(Resource.MODULE_NAME);
    }

    public BaseController(String moduleName) {
        this.moduleName = moduleName;
    }

    public BaseController(String moduleName, String controllerPath, Class<M> modelClass) {
        this.moduleName = moduleName;
        this.controllerPath = controllerPath;
        this.modelClass = modelClass;
    }

    @PostConstruct
    private void init() {
        this.modulePath = this.moduleName;
        Module module = applicationManager.moduleConfiguration().get(moduleName);
        if (module != null) {
            String[] urlPatterns = module.urlPatterns();
            modulePath = (urlPatterns != null && urlPatterns.length > 0) ? urlPatterns[0] : moduleName;
        }
        if (controllerPath == null) {
            RequestMapping annotation = this.getClass().getAnnotation(RequestMapping.class);
            pathNames = annotation != null ? annotation.value() : null;
            controllerPath = pathNames.length > 0 ? pathNames[0] : null;
        }
        if (this.modelClass == null)
            this.modelClass = (Class<M>) GenericTypeResolver.resolveTypeArgument(this.getClass(), BaseController.class);
        if (controllerPath != null) {
            String[] names = controllerPath.replaceAll(PATH_SEPARATOR, "").split("-");
            StringBuilder builder = new StringBuilder();
            Arrays.stream(names).forEach(name -> {
                builder.append(StringUtils.capitalize(name));
            });
            displayName = builder.toString();
        } else
            displayName = this.modelClass != null ? this.modelClass.getSimpleName() : "";
    }

    public Logger getLogger() {
        if (logger == null) {
            System.out.println("LOGGER lazy init");
            logger = LoggerFactory.getLogger(this.getClass());
        }
        return logger;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public M getModel() {
        return model;
    }

    public void setModel(M model) {
        this.model = model;
    }

    public BaseRepository<M> getRepository() {
        return repository;
    }

    @ModelAttribute("controller")
    public BaseController controller() {
        return this;
    }

/*    @ModelAttribute("dataTableController")
    public DataTableBaseController dataTableController() {
        return dataTableController;
    }*/

    @ModelAttribute("navigator")
    public NavigatorBean navigator() {
        return navigatorBean;
    }


    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return index(null, null, null);
    }

    @RequestMapping(value = "{viewId}", method = RequestMethod.GET)
    public String index(@PathVariable("viewId") String viewId, @RequestParam(value = "modelId", required = false) Long modelId, ModelMap modelMap) {
        model = null;
        String view = index(viewId);
        resolveModel(modelId, modelMap);
        return view;
    }

    public abstract String index(String viewId);

    @RequestMapping(value = Resource.CREATE_VALUE, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DataResponse<M> create(@RequestBody @Valid M model) {
        if (model instanceof Flagable) {
            ((Flagable) model).setFlag(Flag.ENABLED);
        }
        validate(model);
        DataResponse dataResponse = new DataResponse(true);
        try {
            getRepository().saveAndFlush(model);
            dataResponse.addMessage(new Message(displayName + " created successfully"));
        } catch (Exception ex) {
            dataResponse.setValid(false);
            dataResponse.addMessage(new Message(displayName + " creation failed", ex.getMessage(), Message.Severity.ERROR));
            getLogger().error(ex.getMessage(), ex);
        }
        return dataResponse;
    }

    @RequestMapping(value = Resource.EDIT_VALUE, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DataResponse<M> edit(@RequestBody @Valid M model) {
        validate(model);
        DataResponse dataResponse = new DataResponse(true);
        try {
            if (model == null || model.getModelId() == null) {
                throw new IllegalArgumentException("Identity not found");
            }
            getRepository().saveAndFlush(model);
            dataResponse.addMessage(new Message(displayName + " modified successfully"));
        } catch (Exception ex) {
            dataResponse.setValid(false);
            dataResponse.addMessage(new Message(displayName + " modification failed", ex.getMessage(), Message.Severity.ERROR));
            getLogger().error(ex.getMessage(), ex);
        }
        return dataResponse;
    }

    @RequestMapping(value = Resource.DELETE_VALUE + "/{modelId}", method = RequestMethod.DELETE)
    @ResponseBody
    public DataResponse<M> delete(@PathVariable("modelId") Long modelId) {
        DataResponse dataResponse = new DataResponse(true);
        try {
            M model = getRepository().findOne(modelId);
            if (model instanceof Flagable) {
                ((Flagable) model).setFlag(Flag.DELETED);
                getRepository().saveAndFlush(model);
            } else {
                getRepository().delete(model);
            }
            dataResponse.addMessage(new Message(displayName + " deleted successfully"));
        } catch (Exception ex) {
            dataResponse.setValid(false);
            dataResponse.addMessage(new Message(displayName + " deletion failed", ex.getMessage(), Message.Severity.ERROR));
            getLogger().error(ex.getMessage(), ex);
        }
        return dataResponse;
    }

    protected void validate(M model) throws DataValidationException {
        Assert.notNull(model, "Validation Model arguement cannot be null");
        if (model instanceof Flagable && ((Flagable) model).getFlag() == null) {
            throw new DataValidationException(new FieldMessage("flag", "Status is required", Message.Severity.ERROR));
        }
    }

    //**********************
    protected String resolveView(String viewId) {
        String viewPath = (modulePath == null || modulePath.equals(PATH_SEPARATOR) ? "" : modulePath + PATH_SEPARATOR)
                + (controllerPath == null || controllerPath.equals(PATH_SEPARATOR) ? "" : controllerPath + PATH_SEPARATOR)
                + viewId;
        if (isAjax()) {
            return viewPath + " :: body";
        }
        navigatorBean.setModuleName(moduleName);
        navigatorBean.setViewPath(viewPath);
        navigatorBean.setViewId(viewId);
        return MAIN_VIEW;
    }

    protected void resolveModel(Long modelId) {
        if (modelId != null) {
            BaseRepository<M> repository = getRepository();
            if (repository != null) {
                model = repository.findOne(modelId);
            }
        }
    }

    protected void resolveModel(Long modelId, ModelMap modelMap) {
        resolveModel(modelId);
        if (modelMap != null) {
            modelMap.addAttribute("model", getModel());
        }
    }

    protected boolean isAjax() {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    /*@RequestMapping(path = "model/list", method = RequestMethod.GET)
    @ResponseBody
    protected DataResponse modelList() {
        return modelList(repository);
    }
*/
    protected <T> DataResponse<T> modelList(FetchableRepository<T> repository) {
        DataResponse dataResponse = new DataResponse();
        try {
            List<Object[]> data = repository == null ? new ArrayList<>() : repository.fetchAllEnabled();
            dataResponse.setData(toNameValuePair(data));
            dataResponse.setValid(true);
        } catch (Exception e) {
            e.printStackTrace();
            dataResponse.addMessage(new Message("Fetching Records did not perform as intended. Please refresh the page", e.getMessage(), Message.Severity.ERROR));
        }
        return dataResponse;
    }

    protected List<NameValuePair> toNameValuePair(List<Object[]> data) {
        if (data == null) {
            return null;
        }
        List<NameValuePair> list = new ArrayList<>();
        for (Object[] ob : data) {
            NameValuePair nvp = new NameValuePair();
            for (int x = 0; x < (ob == null ? 0 : ob.length); x++) {
                String value = ob[x] != null ? ob[x].toString() : null;
                if (x == 0) {
                    nvp.setValue(value);
                } else {
                    nvp.setLabel(nvp.getLabel() != null ? nvp.getLabel() + '-' + value : value);
                }
            }
            list.add(nvp);
        }
        return list;
    }

    /*protected User getCurrentUser() {
        UserPrincipal userPrincipal = getCurrentUserPrincipal();
        if (userPrincipal == null) {
            Principal principal = request != null ? request.getUserPrincipal() : null;
            if (principal instanceof Authentication) {
                Object userObject = ((Authentication) principal).getDetails();
                if (userObject instanceof UserPrincipal)
                    return ((UserPrincipal) userObject).getUser();
                else if (userObject instanceof User)
                    return (User) userObject;
            }
        }
        return userPrincipal != null ? userPrincipal.getUser() : null;
    }*/

   /* protected void setCurrentUser(User user){
        UserPrincipal userPrincipal = getCurrentUserPrincipal();
        if (userPrincipal == null) {
            Principal principal = request != null ? request.getUserPrincipal() : null;
            if (principal instanceof Authentication) {
                Object details = ((Authentication) principal).getDetails();
                if (details instanceof UserPrincipal)
                    return ((UserPrincipal) details).getUser();
            }
        }
    }*/

    /*protected UserPrincipal getCurrentUserPrincipal() {
        UserPrincipal userPrincipal = resolveCurrentUserPrincipal(request != null ? request.getUserPrincipal() : null);
        return userPrincipal != null ? userPrincipal : resolveCurrentUserPrincipal(SecurityContextHolder.getContext().getAuthentication());
    }

    private UserPrincipal resolveCurrentUserPrincipal(Principal principal) {
        if (principal instanceof Authentication) {
            Object _principal = ((Authentication) principal).getPrincipal();
            if (_principal instanceof UserPrincipal) {
                return ((UserPrincipal) _principal);
            }
        }
        return null;
    }*/

    public static final Flag[] statusFlags = {Flag.ENABLED, Flag.DISABLED};
    public static final Flag[] booleanFlags = {Flag.YES, Flag.NO};

    public Flag[] statusFlags() {
        return statusFlags;
    }

    public Flag[] booleanFlags() {
        return booleanFlags;
    }
}


