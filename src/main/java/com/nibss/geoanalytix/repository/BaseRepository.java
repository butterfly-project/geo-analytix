package com.nibss.geoanalytix.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Created by elixir on 5/9/16.
 */
@NoRepositoryBean
public interface BaseRepository<E> extends JpaRepository<E, Long>{
       /* , DataTablesRepository<E, Long> {

    default List<E> findAllBySpecification(DataTablesInput input, Specification<E> additionalSpecification) {
        try {
            Class<E> clazz = (Class<E>) GenericTypeResolver.resolveTypeArgument(getClass(), BaseRepository.class);
            System.out.println("BaseRepository2="+clazz);
            return this.findAll(Specifications.where(DataTablesUtils.getSpecification(clazz, input)).and(additionalSpecification),
                    DataTablesUtils.getPageable(input).getSort());
        } catch (Exception ex) {
            LoggerFactory.getLogger(this.getClass()).error(ex.getMessage(),ex);
        }

        return new ArrayList<>();
    }*/
    /*default List<Object[]> findBy(){
        return null;
    }*/
}
