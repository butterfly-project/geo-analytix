package com.nibss.geoanalytix.repository;

import com.nibss.geoanalytix.entity.State;
import org.springframework.stereotype.Repository;

/**
 * Created by elixir on 8/21/16.
 */
@Repository
public interface StateRepository extends BaseRepository<State> {
}
