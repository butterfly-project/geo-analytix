package com.nibss.geoanalytix.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by elixir on 6/13/16.
 */
@NoRepositoryBean
public interface FetchableRepository<E> extends Repository<E,Long> {

    public List<Object[]> fetchAllEnabled();
}
