package com.nibss.geoanalytix.security;


import com.elixir.collab.springframework.security.WebSecurityConfiguration;
import com.elixir.collab.springframework.web.WebResourceServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * Created by elixir on 5/1/16.
 */


@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfiguration {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
    }
    private static final String[] DELETE_COOKIES = {"JSESSIONID"};
    private static final String LOGIN_PAGE = "/login";
    private static final String[] RESOURCE_LOCATIONS = {"/resources/**", "/webjars/**","/app/**", WebResourceServlet.URL_PATTERN + "*", "/**", LOGIN_PAGE,"/login/**"};

    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);

      /*  http//.anonymous().disable()
                .authorizeRequests()
                .antMatchers(RESOURCE_LOCATIONS).permitAll()
                .antMatchers("*//**").fullyAuthenticated()
                .anyRequest().authenticated()
                .and().rememberMe()
                .and().csrf().disable()
                .formLogin().loginPage(LOGIN_PAGE).permitAll()
                .successHandler(new AuthenticationHandler.ResAuthenticationSuccessHandler())
                .failureHandler(new AuthenticationHandler.AjaxAuthenticationFailureHandler())
                .and()
                .logout().deleteCookies(DELETE_COOKIES)
                .permitAll() ;
*/
    }
}
