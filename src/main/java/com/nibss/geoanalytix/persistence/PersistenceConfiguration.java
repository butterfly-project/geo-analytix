package com.nibss.geoanalytix.persistence;

import com.nibss.geoanalytix.bvn.repository.DemographyRepository;
import com.nibss.geoanalytix.config.AppConstant;
import com.nibss.geoanalytix.repository.BaseRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by oogunjimi on 8/23/2016.
 */

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = AppConstant.PERSISTENCE_UNIT_NAME, transactionManagerRef = AppConstant.PERSISTENCE_UNIT_NAME + "_txManager",basePackageClasses = BaseRepository.class
        /*, repositoryFactoryBeanClass = DataTablesRepositoryFactoryBean.class*/)
public class PersistenceConfiguration {

    @Import(com.elixir.fuse.springframework.persistence.PersistenceConfiguration.class)
    @EnableJpaRepositories( entityManagerFactoryRef = AppConstant.BVN_PERSISTENCE_UNIT_NAME, transactionManagerRef = AppConstant.BVN_PERSISTENCE_UNIT_NAME + "_txManager"
            ,basePackageClasses = DemographyRepository.class)
    public class BvnConfiguration {

    }
}
