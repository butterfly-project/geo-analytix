package com.nibss.geoanalytix;

import com.elixir.fuse.springframework.ApplicationBootInitializer;
import com.elixir.fuse.springframework.config.ConfigProperties;
import com.nibss.geoanalytix.config.AppConfig;
import com.nibss.geoanalytix.config.AppConstant;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by elixir on 8/21/16.
 */
@com.elixir.fuse.springframework.context.Application(name = AppConstant.APP_NAME, rootModuleClass = CoreModuleConfiguration.class)
public class GeoAnalytixApplication  extends ApplicationBootInitializer {

    public static void main(String[] args) {
        System.setProperty(AppConstant.APP_NAME +".home", "E:/projects/workspace/geo-analytix/src/main/resources");
        if (System.getProperty(AppConstant.APP_NAME + ".home") == null)
            throw new IllegalArgumentException();
        WebApplicationContext applicationContext = new GeoAnalytixApplication().run();
    }

    @Override
    protected Class<? extends ConfigProperties> getConfigClass() {
        return AppConfig.class;
    }
}
